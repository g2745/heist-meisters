# Heist Meisters [2021]

Build a Sneaky Dark Survival game [Followed Tutorial].

## Playable Example
[Play Heist Meisters](https://majorvitec.itch.io/heist-meisters)

## Game Overview

### Start Screen

![Start Screen](/images/readme/start_screen.png "Start Screen")

### Tutorial Screen

![Tutorial Screen](/images/readme/tutorial_screen.png "Tutorial Screen")

### Playing Screen

![Level 1 Screen](/images/readme/level_one_screen.png "Level 1 Screen")
![Hide In Box](/images/readme/level_one_screen_box.png "Hide In Box")
![Locked-Door Screen](/images/readme/lockeddoor_screen.png "Locked-Door Screen")
![Computer Screen](/images/readme/computer_screen.png "Computer Screen")

### Victory Screen

![Victory Screen](/images/readme/winning_screen.png "Victory Screen")

### Losing Screen

![Losing Screen](/images/readme/losing_screen.png "Losing Screen")

