extends Node2D


var can_click: bool = false
var combination: Array = []

signal combination

export var combination_length: int = 4
export var lock_group: String = "Unset"


func _ready():
	generate_combination()
	_send_combination_to_locked_door()
	_rotate_computer_text_in_same_direction()
	_set_lock_group_id_in_label()


func _set_lock_group_id_in_label():
	$Label.text = lock_group


func _rotate_computer_text_in_same_direction():
	$Label.rect_rotation = -rotation_degrees


func _send_combination_to_locked_door():
	emit_signal("combination", combination, lock_group)


func generate_combination():
	combination = CombinationGenerator.generate_combination(combination_length)
	set_computerpop_text(combination)


func set_computerpop_text(combination: Array):
	$CanvasLayer/ComputerPopup.set_text(combination)


func open_popup():
	$CanvasLayer/ComputerPopup.popup_centered()


func hide_popup():
	$CanvasLayer/ComputerPopup.hide()


func turn_on_computer_light():
	$Light2D.enabled = true


func turn_off_computer_light():
	$Light2D.enabled = false


func _on_Area2D_body_entered(body:Node):
	can_click = true


func _on_Area2D_body_exited(body:Node):
	can_click = false
	hide_popup()
	turn_off_computer_light()


func _on_Area2D_input_event(viewport:Node, event:InputEvent, shape_idx:int):
	if Input.is_mouse_button_pressed(BUTTON_LEFT) and can_click:
		open_popup()
		turn_on_computer_light()
