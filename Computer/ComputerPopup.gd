extends Popup


onready var computer_label = $NinePatchRect/CenterContainer/NinePatchRect/ComputerTextLabel


func set_text(combination: Array):
    var combination_text: String = PoolStringArray(combination).join("") 

    computer_label.text = (
        "Will you stop forgetting your access code?! \n\nI've set it to "
        + combination_text + 
        ", but this is the very last time!"
    )