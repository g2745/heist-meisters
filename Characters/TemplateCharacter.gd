extends KinematicBody2D

const SPEED: int = 20
const MAX_SPEED: int = 200
const FRICTION: float = 0.1 # time stop moving in seconds