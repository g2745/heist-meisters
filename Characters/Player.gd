extends "res://Characters/TemplateCharacter.gd"


var motion: Vector2 = Vector2()

const PLAYER_SPRITE: String = "res://GFX/PNG/Hitman 1/hitman1_stand.png"
const PLAYER_LIGHT: String = "res://GFX/PNG/Hitman 1/hitman1_stand.png"
const PLAYER_OCCLUDER: String = "res://Characters/HumanOccluder1.tres"

const BOX_SPRITE: String = "res://GFX/PNG/Tiles/tile_156.png"
const BOX_LIGHT: String = "res://GFX/PNG/Tiles/tile_156.png"
const BOX_OCCLUDER: String = "res://Characters/BoxOccluder.tres"

var _velocity_multiplier = 1
var _disguised: bool = false

export var disguise_slowdown: float = 0.25
export var disguise_duration: int = 5
export var number_of_disguises: int = 3


func _ready():
	$Timer.wait_time = disguise_duration
	_send_number_of_disguises_to_gui()
	_reveal()


func _physics_process(delta):
	_update_movements()
	move_and_slide(motion * _velocity_multiplier)

	if _disguised:
		var time_left: String = str($Timer.time_left, 2).pad_decimals(2)
		_set_disquise_label_text(time_left)
		_dont_rotate_disquise_label()



func _update_movements():
	look_at(get_global_mouse_position())

	if Input.is_action_pressed("move_down") and not Input.is_action_just_pressed("move_up"):
		motion.y = clamp(motion.y + SPEED, 0, MAX_SPEED)
	elif Input.is_action_pressed("move_up") and not Input.is_action_just_pressed("move_down"):
		motion.y = clamp(motion.y - SPEED, -MAX_SPEED, 0)
	else:
		motion.y = lerp(motion.y, 0, FRICTION)

	if Input.is_action_pressed("move_right") and not Input.is_action_just_pressed("move_left"):
		motion.x = clamp(motion.x + SPEED, 0, MAX_SPEED)
	elif Input.is_action_pressed("move_left") and not Input.is_action_just_pressed("move_right"):
		motion.x = clamp(motion.x - SPEED, -MAX_SPEED, 0)
	else:
		motion.x = lerp(motion.x, 0, FRICTION)


func _input(event):
	if Input.is_action_just_pressed("toggle_torch"):
		$Torch.enabled = !$Torch.enabled

	if Input.is_action_just_pressed("toggle_nightvision"):
		get_tree().call_group("Interface", "toggle_nightvison_mode")

	if Input.is_action_just_pressed("toggle_disguise"):
		_toggle_disguise()


func _toggle_disguise():
	if _disguised:
		_reveal()
	elif number_of_disguises > 0:
		_disguise()


func _reveal():
	_disguised = false
	_load_player_texture()
	_load_player_light2D_texture()
	_set_player_collision_layer()
	_set_player_occluder()
	_set_movementspeed()
	_hide_disguise_label()


func _disguise():
	_disguised = true
	_load_box_texture()
	_load_box_light2D_texture()
	_set_box_collision_layer()
	_set_box_occluder()
	_set_movementspeed(disguise_slowdown)
	_start_timer()
	_show_disquise_label()
	_substract_disquise_number()
	_send_number_of_disguises_to_gui()


func collect_briefcase():
	var loot: Node = Node.new()
	loot.set_name("Briefcase")

	add_child(loot)
	_show_loot_gui()


func _set_player_collision_layer():
	collision_layer = 1


func _load_player_texture():
	$Sprite.texture = load(PLAYER_SPRITE)


func _set_player_occluder():
	$LightOccluder2D.occluder = load(PLAYER_OCCLUDER)


func _load_player_light2D_texture():
	$Light2D.texture = load(PLAYER_LIGHT)


func _load_box_texture():
	$Sprite.texture = load(BOX_SPRITE)


func _set_box_collision_layer():
	# Layer 5 value = 16
	collision_layer = 16


func _set_box_occluder():
	$LightOccluder2D.occluder = load(BOX_OCCLUDER)


func _load_box_light2D_texture():
	$Light2D.texture = load(BOX_LIGHT)


func _set_movementspeed(speed: float = 1):
	_velocity_multiplier = speed


func _start_timer():
	$Timer.start()


func _hide_disguise_label():
	$DisquiseLabel.hide()


func _show_disquise_label():
	$DisquiseLabel.show()


func _set_disquise_label_text(text: String):
	$DisquiseLabel.text = text


func _dont_rotate_disquise_label():
	$DisquiseLabel.rect_rotation = -rotation_degrees


func _substract_disquise_number():
	number_of_disguises -= 1


func _send_number_of_disguises_to_gui():
	get_tree().call_group("DisguiseDisplay", "update_disguises", number_of_disguises)


func _show_loot_gui():
	get_tree().call_group("Loot", "collect_loot")