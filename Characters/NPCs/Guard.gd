extends "res://Characters/NPCs/PlayerDitection.gd"

onready var navigation = get_tree().get_root().find_node("Navigation2D", true, false)
onready var destination = navigation.get_node("Destinations")

var motion: Vector2
var possible_destinations: Array
var path: PoolVector2Array

export var minimum_arrival_distance: int = 5
export var walk_speed: float = 0.5


func _ready():
	randomize()
	possible_destinations = destination.get_children()
	make_path()


func _physics_process(delta):
	navigate()


func navigate():
	# handling: am i at my destination or should i keep moving
	var distance_to_destination: int = position.distance_to(path[0])

	if distance_to_destination > minimum_arrival_distance:
		move()
	else:
		update_path()


func move():
	look_at(path[0])
	motion = (path[0] - position).normalized() * (MAX_SPEED * walk_speed)

	if is_on_wall():
		make_path()

	move_and_slide(motion)


func update_path():
	if path.size() == 1:
		if $Timer.is_stopped():
			$Timer.start()
	else:
		path.remove(0)


func make_path():
	var random_destination: int = randi() % possible_destinations.size() - 1
	var new_destination: Node = possible_destinations[random_destination]
	path = navigation.get_simple_path(position, new_destination.position, false)


func _on_Timer_timeout():
	make_path()
