extends "res://Characters/TemplateCharacter.gd"

const FOV_TOLERANCE: int = 20 # degreeeeees
const MAX_DECTECTION_RANGE: int = 640

const RED: Color = Color(1, 0.25, 0.25)
const WHITE: Color = Color(1, 1, 1)

var Player: Node

func _ready():
	Player = get_node("/root").find_node("Player", true, false)


func _process(delta):
	if player_in_fov() and player_in_los():
		$Torch.color = RED
		get_tree().call_group("SuspicionMeter", "player_seen")
	else:
		$Torch.color = WHITE


# Field of View
func player_in_fov():
	var npc_facing_direction: Vector2 = Vector2(1, 0).rotated(global_rotation)
	var direction_to_Player:Vector2 = (Player.position - global_position).normalized()

	if abs(direction_to_Player.angle_to(npc_facing_direction)) < deg2rad(FOV_TOLERANCE):
		return true

	return false


# Line of Sight
func player_in_los(): 
	# getting the worlds physics space
	# allows to do arbitrary queries for collision
	var space = get_world_2d().direct_space_state
	var los_obstacle = space.intersect_ray(global_position, Player.global_position, [self], collision_mask)

	if not los_obstacle:
		return false
		
	var dinstance_to_player = Player.global_position.distance_to(global_position)
	var player_in_range = dinstance_to_player < MAX_DECTECTION_RANGE

	if los_obstacle.collider == Player and player_in_range:
		return true

	return false
