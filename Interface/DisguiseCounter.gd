extends ItemList


const BOX_SPRITE: String = "res://GFX/PNG/Tiles/tile_129.png"


func update_disguises(number: int):
    clear()

    for disguises in range(number):
        add_icon_item(load(BOX_SPRITE), false)