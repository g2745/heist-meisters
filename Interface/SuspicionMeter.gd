extends TextureProgress

const LOSING_SCENE: String = "res://Levels/LosingScreen.tscn"

export var suspicion_multiplier: int = 2


func _ready():
    value = 0


func _process(delta):
    value -= step


func player_seen():
    value += step * suspicion_multiplier
    
    if value == max_value:
        end_game()


func end_game():
    _load_losing_scene()


func _load_losing_scene():
    get_tree().change_scene(LOSING_SCENE)