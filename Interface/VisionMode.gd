extends CanvasModulate


const NICHTVISION_OFF_SOUND: String = "res://SFX/nightvision_off.wav"
const NICHTVISION_ON_SOUND: String = "res://SFX/nightvision.wav"
const DARK: Color = Color("111111")
const NIGHTVISON: Color = Color("37bf62")

var cooldown: bool = false


func _ready():
	visible = true
	color = DARK


func toggle_nightvison_mode():
	if not cooldown:
		if color == DARK:
			nightvision_mode()
		else:
			dark_mode()
		
		cooldown = true
		$Timer.start()


func _play_nightvision_off_sound():
	$NightvisonASP2D.stream = load(NICHTVISION_OFF_SOUND)
	$NightvisonASP2D.play()


func _play_nightvision_on_sound():
	$NightvisonASP2D.stream = load(NICHTVISION_ON_SOUND)
	$NightvisonASP2D.play()


func _turn_off_lables():
	get_tree().call_group("labels", "hide")


func _turn_on_lables():
	get_tree().call_group("labels", "show")


func _turn_off_lights():
	get_tree().call_group("lights", "hide")


func _turn_on_lights():
	get_tree().call_group("lights", "show")


func dark_mode():
	color = DARK
	_play_nightvision_off_sound()
	_turn_off_lables()
	_turn_on_lights()


func nightvision_mode():
	color = NIGHTVISON
	_play_nightvision_off_sound()
	_turn_on_lables()
	_turn_off_lights()


func _on_Timer_timeout():
	cooldown = false
