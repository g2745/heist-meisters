extends Popup


var combination: Array = []
var guess = []

onready var display = $VBoxContainer/DisplayContainer/Display
onready var light = $VBoxContainer/ButtonContainer/GridContainer/StatusLight

signal combination_correct


func play_check_sound():
	$AudioStreamPlayer.stream = load("res://SFX/twoTone1.ogg")
	$AudioStreamPlayer.play()


func play_correct_sound():
	$AudioStreamPlayer.stream = load("res://SFX/threeTone1.ogg")
	$AudioStreamPlayer.play()


func set_light_to_green():
	light.texture = load("res://GFX/Interface/PNG/dotGreen.png")


func set_light_to_red():
	light.texture = load("res://GFX/Interface/PNG/dotRed.png")


func _ready():
	connect_buttons()
	reset_lock()


func connect_buttons():
	var button_grid_container: Array = $VBoxContainer/ButtonContainer/GridContainer.get_children()
	
	for child in button_grid_container:
		if child is Button:
			child.connect("pressed", self, "button_pressed", [child.text])


func button_pressed(button_text: String):
	if button_text == "OK":
		check_guess()
	else:
		enter(int(button_text))


func check_guess():
	if guess == combination:
		play_correct_sound()
		set_light_to_green()
		$Timer.start()
	else:
		reset_lock()


func enter(button_number: int):
	play_check_sound()
	guess.append(button_number)
	update_display()


func update_display():
	display.text = PoolStringArray(guess).join("")

	if guess.size() == combination.size():
		check_guess()


func reset_lock():
	set_light_to_red()
	display.text = ""
	guess.clear()


func _on_Timer_timeout():
	emit_signal("combination_correct")
	reset_lock()
