extends NinePatchRect


const BRIEFCASE_SPRITE: String = "res://GFX/Loot/suitcase.png"


func _ready():
    hide()


func collect_loot():
    show()
    _load_briefcase_icon()


func _load_briefcase_icon():
     $VBoxContainer/ItemList.add_icon_item(load(BRIEFCASE_SPRITE), false)