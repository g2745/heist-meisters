extends Area2D


var can_click: bool = false


func is_player(body: Node):
    # Collision Layer value 1 is set to Player
    if body.collision_layer == 1:
        return true

    return false


func _on_Door_body_entered(body:Node):
    if is_player(body): 
        can_click = true;
    else:
        open()


func open():
    $AnimationPlayer.play("Open")


func _on_Door_body_exited(body:Node):
    if is_player(body):
        can_click = false


func _on_Door_input_event(viewport:Node, event:InputEvent, shape_idx:int):
    if Input.is_mouse_button_pressed(BUTTON_LEFT) and can_click:
        open()
