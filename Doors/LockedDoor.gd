extends "res://Doors/Door.gd"


func _ready():
	_rotate_door_text_in_same_direction()


func _rotate_door_text_in_same_direction():
	$Label.rect_rotation = -rotation_degrees


func open_popup():
	$CanvasLayer/Numpad.popup_centered()


func hide_popup():
	$CanvasLayer/Numpad.hide()


func _on_Door_body_exited(body:Node):
	if is_player(body):
		can_click = false
		hide_popup()


func _on_Door_input_event(viewport:Node, event:InputEvent, shape_idx:int):
	if Input.is_mouse_button_pressed(BUTTON_LEFT) and can_click:
		open_popup()


func _on_Numpad_combination_correct():
	open()
	hide_popup()


func _set_numpad_combination(combination: Array):
	$CanvasLayer/Numpad.combination = combination


func _set_door_label_lock_group(lock_group: String):
	$Label.text = lock_group


func _on_Computer_combination(combination: Array, lock_group: String):
	_set_numpad_combination(combination)
	_set_door_label_lock_group(lock_group)
