extends Node2D


func _ready():
    # _wait_one_frame()
    _update_objective(0)


func _update_objective(objective_number: int):
    _move_pointer(objective_number)
    _play_message_sound()
    _set_objective_message(objective_number)
    _play_message_box_animation()


func _on_MoveObjective_body_entered(body:Node):
    _update_objective(1)


func _on_DoorObjective_body_entered(body:Node):
    _activate_nightvision_mode()
    _update_objective(2)


func _on_NightvisionObjective_body_entered(body:Node):
    _activate_dark_mode()
    _update_objective(3)


func _on_BriefcaseObjective_body_entered(body:Node):
    _update_objective(4)


func _wait_one_frame():
    yield(get_tree(), "idle_frame")


func _move_pointer(objective_number: int):
    var pointer: Node = $ObjectivePointer
    var place: Node = $ObjectivePositions.get_child(objective_number)

    $Tween.interpolate_property(
        pointer, "position", pointer.position, place.position, 0.5, Tween.TRANS_SINE, Tween.EASE_IN_OUT
    )
    $Tween.start()


func _set_objective_message(objective_number: int):
    var messageNode: Node = $ObjectiveMessages.get_child(objective_number)
    $TutorialGUI/Control/NinePatchRect/Label.text = messageNode.message


func _play_message_sound():
    $MessageSound.play()


func _play_message_box_animation():
    $TutorialGUI/Control/AnimationPlayer.play("MessageAnimation")


func _activate_nightvision_mode():
    get_tree().call_group("Interface", "nightvision_mode")


func _activate_dark_mode():
    get_tree().call_group("Interface", "dark_mode")