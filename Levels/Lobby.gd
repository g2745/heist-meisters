extends Control


const LEVEL_ONE_SCENE: String = "res://Levels/Level1.tscn"
const TUTORIAL_SCENE: String = "res://Levels/Tutorial.tscn"


func _on_QuitButton_pressed():
    get_tree().quit()


func _on_StartButton_pressed():
    _load_level_one_scene() 


func _on_TutorialButton_pressed():
    _load_tutorial_scene() 


func _end_game():
    get_tree().quit()


func _load_level_one_scene():
    get_tree().change_scene(LEVEL_ONE_SCENE)


func _load_tutorial_scene():
    get_tree().change_scene(TUTORIAL_SCENE)
