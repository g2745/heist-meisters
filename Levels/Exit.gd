extends ColorRect


const VICTORY_SCREEN: String = "res://Levels/VictoryScreen.tscn"
const LOOT: String = "Briefcase"


func _on_Area2D_body_entered(body: Node):
    if _player_has_briefcase(body):
        _end_game()
    else:
        print("You dont have a Briefcase Yet")


func _player_has_briefcase(body: Node):
    if body.has_node(LOOT):
        return true

    return false


func _end_game():
    _load_victory_screen()


func _load_victory_screen():
    get_tree().change_scene(VICTORY_SCREEN)
